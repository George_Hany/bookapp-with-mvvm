package com.example.android.myapplication2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.example.android.myapplication2.adapter.BooksAdapter
import com.example.android.myapplication2.databinding.ActivityMainBinding
import com.example.android.myapplication2.vmodel.BookViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        var bookViewModel = ViewModelProviders.of(this).get(BookViewModel::class.java)
        binding.vm = bookViewModel
//        AndroidNetworking.initialize(getApplicationContext())
        bookViewModel.items.observe(this, Observer {
            var adapter = BooksAdapter(it!!)
            books_recycle.adapter=adapter
//            binding.booksRecycle.layoutManager = LinearLayoutManager(this)
//            binding.booksRecycle.adapter = adapter
//                books_recycle.adapter=BooksAdapter(it!!)
//                books_recycle.layoutManager=LinearLayoutManager(this@MainActivity)

        })

    }

}
