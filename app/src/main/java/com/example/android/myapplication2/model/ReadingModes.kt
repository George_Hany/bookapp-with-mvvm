package com.example.android.myapplication2.model

data class ReadingModes(
    val image: Boolean,
    val text: Boolean
)