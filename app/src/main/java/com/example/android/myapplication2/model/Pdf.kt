package com.example.android.myapplication2.model

data class Pdf(
    val isAvailable: Boolean
)