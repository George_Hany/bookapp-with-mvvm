package com.example.android.myapplication2.vmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class MyViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(BookViewModel::class.java!!)) {
            BookViewModel() as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}