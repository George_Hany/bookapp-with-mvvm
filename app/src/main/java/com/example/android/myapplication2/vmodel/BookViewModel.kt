package com.example.android.myapplication2.vmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.myapplication2.adapter.BooksAdapter
import com.example.android.myapplication2.model.Book
import com.example.android.myapplication2.model.RootResponse
import com.rxandroidnetworking.RxAndroidNetworking
import rx.Observable
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

open class BookViewModel : ViewModel() {
    var word: String = ""
    var items = MutableLiveData<ArrayList<Book>>()
    var adapter = BooksAdapter(ArrayList())

    fun fastSearch() {
        var observable =
            RxAndroidNetworking.get("https://www.googleapis.com/books/v1/volumes").addQueryParameter("q", word).build()
                .getObjectObservable(RootResponse::class.java)

        observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RootResponse> {
                override fun onCompleted() {
                    Log.e("errrr","done")
                }

                override fun onNext(t: RootResponse) {
                    items.value = t!!.items

                }

                override fun onError(e: Throwable) {
                    Log.e("error", e.message)
                }
            })


//        RxAndroidNetworking.get("https://fierce-cove-29863.herokuapp.com/getAnUser/{userId}")
//            .addPathParameter("userId", "1")
//            .build()
//            .getObjectObservable<RootResponse>(RootResponse::class.java) // This returns you an Observable
//            .subscribeOn(Schedulers.io()) // do the network call on another thread
//            .observeOn(AndroidSchedulers.mainThread()) // return the result in mainThread
//            .map(Func1<Any, Any> { apiUser ->
//                // here we get ApiUser from server
//                // then by converting, we are returing user
//                convert(apiUser)
//            })
//            .subscribe(object : Observer<User> {
//                fun onCompleted() {
//                    // do anything onComplete
//                }
//
//                override fun onError(e: Throwable) {
//                    // handle error
//                }
//
//                override fun onNext(user: User) {
//                    // do anything with user
//                }
//            })

    }


}


