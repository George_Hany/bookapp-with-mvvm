package com.example.android.myapplication2.model

data class IndustryIdentifier(
    val identifier: String,
    val type: String
)