package com.example.android.myapplication2.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android.myapplication2.R
import com.example.android.myapplication2.databinding.BookItemBinding
import com.example.android.myapplication2.model.Book

class BooksAdapter (var books:ArrayList<Book>) :RecyclerView.Adapter<BooksAdapter.Holder>(){
//    var books=books
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val v = LayoutInflater.from(parent.context)
        val binding:BookItemBinding=DataBindingUtil.inflate(v,R.layout.book_item,parent,false)
        val h = Holder(binding)
        return h
    }

    override fun getItemCount(): Int =  books.size

    override fun onBindViewHolder(holder: Holder, index: Int) {
//        holder.title.setText(books.get(index).volumeInfo.title)
//        holder.authors.setText(books?.get(index)?.volumeInfo?.authors!![0]+"")
//        holder.publishedDate.setText(books.get(index).volumeInfo.publishedDate)
//        Glide.with(holder.itemView.context).load("http://books.google.com/books/content?id=${books.get(index).id}&printsec=frontcover&img=1&zoom=1&source=gbs_api").into(holder.book_image)
        holder.bind(books.get(index))
    }

    class Holder(var viewin: BookItemBinding) :RecyclerView.ViewHolder(viewin.root) {
//        val book_image=view.book_image
//        val title =view.title
//        val authors=view.authors
//        val publishedDate=view.publishedDate
        fun bind(book:Book){
            viewin.book=book
            viewin.image=book.volumeInfo.imageLinks.thumbnail
        }

    }

}