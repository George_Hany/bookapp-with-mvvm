package com.example.android.myapplication2.model

 class Book(
    val accessInfo: AccessInfo,
    val etag: String,
    val id: String,
    val kind: String,
    val saleInfo: SaleInfo,
    val selfLink: String,
    val volumeInfo: VolumeInfo
){
     companion object{
         fun convert(book:Book):MyBook{
             val myBook=MyBook(book.id,book.volumeInfo.title)
             return myBook
         }
     }
 }