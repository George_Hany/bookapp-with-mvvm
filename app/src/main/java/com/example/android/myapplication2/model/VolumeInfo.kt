package com.example.android.myapplication2.model

data class VolumeInfo(
    val allowAnonLogging: Boolean,
    var authors: List<String>?= listOf(""),
    val canonicalVolumeLink: String,
    val categories: List<String>,
    val contentVersion: String,
    val imageLinks: ImageLinks,
    val industryIdentifiers: List<IndustryIdentifier>,
    val infoLink: String,
    val language: String,
    val maturityRating: String,
    val previewLink: String,
    val printType: String,
    var publishedDate: String?="",
    val readingModes: ReadingModes,
    var title: String
)