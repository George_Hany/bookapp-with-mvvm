package com.example.android.myapplication2.model

import com.google.gson.annotations.SerializedName

class RootResponse {
    @SerializedName("items")
    lateinit var items: ArrayList<Book>
}