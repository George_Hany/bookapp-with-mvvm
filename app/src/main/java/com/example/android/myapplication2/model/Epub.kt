package com.example.android.myapplication2.model

data class Epub(
    val isAvailable: Boolean
)