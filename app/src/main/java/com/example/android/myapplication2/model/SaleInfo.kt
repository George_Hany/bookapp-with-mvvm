package com.example.android.myapplication2.model

data class SaleInfo(
    val country: String,
    val isEbook: Boolean,
    val saleability: String
)