package com.example.android.myapplication2

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

class CustomImage {
    companion object{
        @BindingAdapter("android:imagePath")
        @JvmStatic
        public fun setimagePath(image:ImageView,path:String){
            Glide.with(image.context).load(path).into(image)
        }
    }

}