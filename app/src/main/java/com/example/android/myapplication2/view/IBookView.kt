package com.example.android.myapplication2.view

import com.example.android.myapplication2.model.Book

interface IBookView {
    fun onRecieveBooks(books:ArrayList<Book>)
    fun onError(msg:String)
}